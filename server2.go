package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
)

type Users struct {
	Nama  string `json:"Deploy"`
	Kelas string `json:"Kelas"`
}

func getUser(w http.ResponseWriter, r *http.Request) {
	var users Users // variable untuk memetakan data product yang terbagi menjadi 3 field

	var arr_users []Users
	db, err := sql.Open("mysql", "fin:123@tcp(127.0.0.1:3306)/go_cicd")
	defer db.Close()

	if err != nil {
		log.Fatal(err)
	}
	rows, err := db.Query("Select nama_deploy,kelas_deploy from identitas ORDER BY nama_deploy DESC")
	if err != nil {
		log.Print(err)
	}
	count := 0
	for rows.Next() {
		if err := rows.Scan(&users.Nama, &users.Kelas); err != nil {
			log.Fatal(err.Error())

		} else {
			arr_users = append(arr_users, users)
			fmt.Println(arr_users[count])
		}
		count++
	}

	json.NewEncoder(w).Encode(arr_users)

}

func main() {
	router := mux.NewRouter()
	router.HandleFunc("/getUser", getUser).Methods("GET") // menjalurkan URL untuk dapat mengkases data JSON API product ke /getproducts
	http.Handle("/", router)
	log.Fatal(http.ListenAndServe(":8080", router))
}
